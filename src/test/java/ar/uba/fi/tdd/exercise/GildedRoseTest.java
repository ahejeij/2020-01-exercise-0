package ar.uba.fi.tdd.exercise;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class GildedRoseTest {

	@Test
	public void normalItemLosesQualityAndSellInByOne() {
		Item[] items = new Item[] { new Item("Item 1", 10, 10) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(app.items[0].quality).isEqualTo(9);
		assertThat(app.items[0].sellIn).isEqualTo(9);
	}

	@Test
	public void normalItemDoesntLoseQualityIfZero() {
		Item[] items = new Item[] { new Item("Item 1", 10, 0) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(app.items[0].quality).isEqualTo(0);
		assertThat(app.items[0].sellIn).isEqualTo(9);
	}

	@Test
	public void normalItemSellDatePassedLosesQualityBy2() {
		Item[] items = new Item[] { new Item("Item 1", 0, 10) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(app.items[0].quality).isEqualTo(8);
		assertThat(app.items[0].sellIn).isEqualTo(-1);
	}

	@Test
	public void sulfurasDoesntLoseQualityNorSellIn() {
		Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 10, 10) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(app.items[0].quality).isEqualTo(10);
		assertThat(app.items[0].sellIn).isEqualTo(10);
	}

	@Test
	public void backstageDatePassedLosesAllQuality() {
		Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 0, 20) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(app.items[0].quality).isEqualTo(0);
		assertThat(app.items[0].sellIn).isEqualTo(-1);
	}

	@Test
	public void backstageDateWith8DaysLeftGains2Quality() {
		Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 8, 20) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(app.items[0].quality).isEqualTo(22);
		assertThat(app.items[0].sellIn).isEqualTo(7);
	}

	@Test
	public void backstageDateWith5DaysLeftGains3Quality() {
		Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 5, 20) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(app.items[0].quality).isEqualTo(23);
		assertThat(app.items[0].sellIn).isEqualTo(4);
	}

	@Test
	public void itemQualityDoesNotPass50(){
		Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 5, 49) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(app.items[0].quality).isEqualTo(50);
		assertThat(app.items[0].sellIn).isEqualTo(4);
	}

	@Test
	public void agedBrieGains1QualityWhenDaysNotPassed(){
		Item[] items = new Item[] { new Item("Aged Brie", 5, 20) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(app.items[0].quality).isEqualTo(21);
		assertThat(app.items[0].sellIn).isEqualTo(4);
	}

	@Test
	public void agedBrieStillGains1QualityWhenDaysHavePassed(){
		Item[] items = new Item[] { new Item("Aged Brie", -1, 20) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(app.items[0].quality).isEqualTo(22);
		assertThat(app.items[0].sellIn).isEqualTo(-2);
	}

	@Test
	public void conjuredLoses2QualityWhenSellDateHasNotPassed() {
		Item[] items = new Item[] { new Item("Conjured", 10, 10) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(app.items[0].quality).isEqualTo(8);
		assertThat(app.items[0].sellIn).isEqualTo(9);
	}

	@Test
	public void conjuredLoses4QualityWhenSellDateHasPassed() {
		Item[] items = new Item[] { new Item("Conjured", 0, 10) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(app.items[0].quality).isEqualTo(6);
		assertThat(app.items[0].sellIn).isEqualTo(-1);
	}
}
