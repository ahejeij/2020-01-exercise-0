package ar.uba.fi.tdd.exercise;

class GildedRose {
  Item[] items;

    public GildedRose(Item[] _items) {
        items = _items;
    }

    private boolean itemIsAgedBrie(Item item){
        return item.Name.equals("Aged Brie");
    }

    private boolean itemIsBackStage(Item item){
        return item.Name.equals("Backstage passes to a TAFKAL80ETC concert");
    }

    private boolean itemIsSulfuras(Item item){
        return item.Name.equals("Sulfuras, Hand of Ragnaros");
    }

    private boolean itemIsConjured(Item item){
        return item.Name.equals("Conjured");
    }

    private void reduceItemSellIn(Item item){
        item.sellIn -= 1;
    }

    public void updateQuality() {
        QualityUpdater qualityUpdater = new QualityUpdater();
        for (Item item: items) {
            if (itemIsSulfuras(item)) continue;
            reduceItemSellIn(item);
            if (itemIsAgedBrie(item)) {
                qualityUpdater.updateAgedBrie(item);
                continue;
            }
            if (itemIsBackStage(item)) {
                qualityUpdater.updateBackstage(item);
                continue;
            }
            if(itemIsConjured(item)){
                qualityUpdater.updateConjured(item);
                continue;
            }
            qualityUpdater.updateNormalItem(item);
        }
    }
}
