package ar.uba.fi.tdd.exercise;

public class QualityUpdater {

    private int MAX_QUALITY = 50;
    private int MIN_QUALITY = 0;
    private int itemGetNewQuality(int currentQuality, int qualityToAdd){
        if (currentQuality + qualityToAdd > MAX_QUALITY) return MAX_QUALITY;
        if (currentQuality + qualityToAdd < MIN_QUALITY) return MIN_QUALITY;
        return currentQuality + qualityToAdd;
    }

    private boolean itemSellDateHasPassed(Item item){
        return item.sellIn < 0;
    }

    private boolean itemHas10OrLessDaysToSell(Item item){ return item.sellIn <= 10;}
    private boolean itemHas5OrLessDaysToSell(Item item){ return item.sellIn <= 5;}

    public void updateAgedBrie(Item agedBrie){
        agedBrie.quality = itemSellDateHasPassed(agedBrie) ?
                itemGetNewQuality(agedBrie.quality, 2) : itemGetNewQuality(agedBrie.quality, 1);
    }

    public void updateBackstage(Item backstage){
        backstage.quality =  itemHas10OrLessDaysToSell(backstage)?
                itemGetNewQuality(backstage.quality, 2) : itemGetNewQuality(backstage.quality, 1);

        backstage.quality = itemHas5OrLessDaysToSell(backstage) ?
                itemGetNewQuality(backstage.quality, 1) : backstage.quality;

        backstage.quality = itemSellDateHasPassed(backstage) ?
                0 : backstage.quality;
    }

    public void updateNormalItem(Item item){
        item.quality = itemSellDateHasPassed(item) ?
                itemGetNewQuality(item.quality, -2) : itemGetNewQuality(item.quality, -1);
    }

    public void updateConjured(Item item){
        item.quality = itemSellDateHasPassed(item) ?
                itemGetNewQuality(item.quality, -4) : itemGetNewQuality(item.quality, -2);
    }
}
